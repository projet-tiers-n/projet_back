<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use App\Repository\InventeurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: InventeurRepository::class)]
#[ApiResource(paginationType: 'page')]
#[ApiFilter(SearchFilter::class, properties: ['name' => 'partial', 'pays' => 'exact'])]
class Inventeur
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['inventions:read:list', 'inventeurs:read:list', 'inventions:read:item', 'inventeurs:read:item', 'inventeurs:read:id'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['inventions:read:list', 'inventeurs:read:list', 'inventions:read:item', 'inventeurs:read:item'])]
    private ?string $name = null;

    #[ORM\ManyToMany(targetEntity: Invention::class, mappedBy: 'inventeur')]
    #[Groups(['inventeurs:read:item'])]
    private Collection $inventions;

    #[ORM\Column(length: 2)]
    #[Groups(['inventeurs:read:list', 'inventions:read:item', 'inventeurs:read:item'])]
    private ?string $pays = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(['inventeurs:read:item'])]
    private ?string $description = null;

    public function __construct()
    {
        $this->inventions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Invention>
     */
    public function getInventions(): Collection
    {
        return $this->inventions;
    }

    public function addInvention(Invention $invention): self
    {
        if (!$this->inventions->contains($invention)) {
            $this->inventions->add($invention);
            $invention->addInventeur($this);
        }

        return $this;
    }

    public function removeInvention(Invention $invention): self
    {
        if ($this->inventions->removeElement($invention)) {
            $invention->removeInventeur($this);
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPays(): ?string
    {
        return $this->pays;
    }

    /**
     * @param string|null $pays
     */
    public function setPays(?string $pays): void
    {
        $this->pays = $pays;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
