<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use App\Repository\CategorieRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: CategorieRepository::class)]
#[ApiResource(paginationType: 'page')]
#[ApiFilter(SearchFilter::class, properties: ['name' => 'partial'])]
class Categorie
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['inventions:read:list', 'inventeurs:read:list', 'categories:read:list', 'categories:read:item', 'categories:read:ids', 'inventions:read:item', 'inventeurs:read:item'])]
    private ?int $id = null;

    #[ORM\Column(length: 50)]
    #[Groups(['inventions:read:list', 'categories:read:list', 'categories:read:item', 'inventions:read:item', 'inventeurs:read:item'])]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'categorie', targetEntity: Invention::class)]
    private Collection $inventions;

    public function __construct()
    {
        $this->inventions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Invention>
     */
    public function getInventions(): Collection
    {
        return $this->inventions;
    }

    public function addInvention(Invention $invention): self
    {
        if (!$this->inventions->contains($invention)) {
            $this->inventions->add($invention);
            $invention->setCategorie($this);
        }

        return $this;
    }

    public function removeInvention(Invention $invention): self
    {
        if ($this->inventions->removeElement($invention)) {
            // set the owning side to null (unless already changed)
            if ($invention->getCategorie() === $this) {
                $invention->setCategorie(null);
            }
        }

        return $this;
    }
}
