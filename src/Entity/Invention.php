<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use App\Repository\InventionsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: InventionsRepository::class)]
#[ApiResource(paginationType: 'page')]
#[ApiFilter(OrderFilter::class, properties: ['name' => 'ASC', "annee" => "ASC"])]
#[ApiFilter(SearchFilter::class, properties: ['name' => 'partial', 'categorie.name' => 'exact', 'inventeur.name' => 'partial'])]
class Invention
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['inventions:read:list', 'inventions:read:item', 'inventeurs:read:item', 'inventions:read:id', 'inventions:ajout'])]
    private ?int $id = null;

    #[ORM\Column(length: 50)]
    #[Groups(['inventions:read:list', 'inventions:read:item', 'inventeurs:read:item'])]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    #[Groups(['inventions:read:item'])]
    private ?string $description = null;

    #[ORM\ManyToOne(inversedBy: 'inventions')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['inventions:read:list', 'inventions:read:item', 'inventeurs:read:item'])]
    private ?Categorie $categorie = null;

    #[ORM\ManyToMany(targetEntity: Inventeur::class, inversedBy: 'inventions')]
    #[Groups(['inventions:read:list', 'inventions:read:item'])]
    private Collection $inventeur;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    #[Groups(['inventions:read:list', 'inventions:read:item', 'inventeur:read:item'])]
    private ?\DateTimeInterface $annee = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['inventions:read:item'])]
    private ?string $image = null;

    public function __construct()
    {
        $this->inventeur = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCategorie(): ?Categorie
    {
        return $this->categorie;
    }

    public function setCategorie(?Categorie $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * @return Collection<int, Inventeur>
     */
    public function getInventeur(): Collection
    {
        return $this->inventeur;
    }

    public function addInventeur(Inventeur $inventeur): self
    {
        if (!$this->inventeur->contains($inventeur)) {
            $this->inventeur->add($inventeur);
        }

        return $this;
    }

    public function removeInventeur(Inventeur $inventeur): self
    {
        $this->inventeur->removeElement($inventeur);

        return $this;
    }

    public function removeAllInventeur(): self
    {
        $this->inventeur->clear();

        return $this;
    }

    public function getAnnee(): ?\DateTimeInterface
    {
        return $this->annee;
    }

    public function setAnnee(\DateTimeInterface $annee): self
    {
        $this->annee = $annee;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }
}
