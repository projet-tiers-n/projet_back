<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Invention;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Invention>
 *
 * @method Invention|null find($id, $lockMode = null, $lockVersion = null)
 * @method Invention|null findOneBy(array $criteria, array $orderBy = null)
 * @method Invention[]    findAll()
 * @method Invention[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InventionsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Invention::class);
    }

    public function save(Invention $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Invention $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
    public function findByCategorieAndInventeur(int|null $categorie, int|null $inventeur)
    {
        $qb = $this->createQueryBuilder('i');
        if ($categorie) {
            $qb->andWhere('i.categorie = :categorie')
                ->setParameter('categorie', $categorie);
        }
        if ($inventeur) {
            $qb->andWhere('i.inventeur = :inventeur')
                ->setParameter('inventeur', $inventeur);
        }
        return $qb->getQuery()->getResult();
    }

    public function findById(int|array $id)
    {
        $qb = $this->createQueryBuilder('i');
        if (is_array($id)) {
            $qb->andWhere('i.id IN (:id)')
                ->setParameter('id', $id);
        } else {
            $qb->andWhere('i.id = :id')
                ->setParameter('id', $id);
        }
        return $qb->getQuery()->getResult();
    }

//    /**
//     * @return Inventions[] Returns an array of Inventions objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('i')
//            ->andWhere('i.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('i.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Inventions
//    {
//        return $this->createQueryBuilder('i')
//            ->andWhere('i.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }

}
