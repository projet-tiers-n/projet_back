<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Inventeur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Inventeur>
 *
 * @method Inventeur|null find($id, $lockMode = null, $lockVersion = null)
 * @method Inventeur|null findOneBy(array $criteria, array $orderBy = null)
 * @method Inventeur[]    findAll()
 * @method Inventeur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InventeurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Inventeur::class);
    }

    public function save(Inventeur $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Inventeur $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findByPays(string|null $pays)
    {
        $qb = $this->createQueryBuilder('i');
        if ($pays) {
            $qb->andWhere('i.pays = :pays')
                ->setParameter('pays', $pays);
        }
        return $qb->getQuery()->getResult();
    }

    public function findById(string|array $id)
    {

        $qb = $this->createQueryBuilder('i');
        if (is_array($id)) {

            $qb->andWhere('i.id IN (:id)')
                ->setParameter('id', $id);
        } else {
            $qb->andWhere('i.id = :id')
                ->setParameter('id', $id);
        }

        return $qb->getQuery()->getResult();
    }

//    /**
//     * @return Inventeur[] Returns an array of Inventeur objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('i')
//            ->andWhere('i.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('i.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Inventeur
//    {
//        return $this->createQueryBuilder('i')
//            ->andWhere('i.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
