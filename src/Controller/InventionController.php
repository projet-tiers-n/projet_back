<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Invention;
use App\Repository\CategorieRepository;
use App\Repository\InventeurRepository;
use App\Repository\InventionsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('v1/inventions')]
class InventionController extends AbstractController
{
    #[Route('/', name: 'app_list_inventions', methods: ['GET'])]
    public function listInventions(Request $request, InventionsRepository $inventionsRepo): JsonResponse
    {
        $categorie = $request->query->get('categorie');
        $inventeur = $request->query->get('inventeur');
        return $this->json($inventionsRepo->findByCategorieAndInventeur($categorie, $inventeur), 200, [], ['groups' => 'inventions:read:list']);
    }

    #[Route('/ids', name: 'app_ids_inventions', methods: ['GET'])]
    public function getIdsInventions(InventionsRepository $inventionsRepo): JsonResponse
    {
        return $this->json($inventionsRepo->findAll(), 200, [], ['groups' => 'inventions:read:id']);
    }

    #[Route('/info-edit/{id}', name: 'app_infoEdit_inventions', methods: ['GET'])]
    public function getInfoEditInventions(InventionsRepository $inventionsRepo, InventeurRepository $inventeurRepo, CategorieRepository $categorieRepo, int $id): JsonResponse
    {
        return $this->json([
            'invention' => $inventionsRepo->find($id),
            'inventeurs' => $inventeurRepo->findAll(),
            'categories' => $categorieRepo->findAll(),
        ], context: ['groups' => 'inventions:read:item']);
    }

    #[Route('/{id}', name: 'app_get_invention', methods: ['GET'])]
    public function getInvention(InventionsRepository $inventionsRepository, int $id): JsonResponse
    {
        return $this->json($inventionsRepository->find($id), 200, [], ['groups' => 'inventions:read:item']);
    }

    /**
     * @throws \Exception
     */
    #[Route('/', name: 'app_add_inventions', methods: ['POST'])]
    public function addInventions(Request $request, InventionsRepository $inventionsRepository, InventeurRepository $inventeurRepository, CategorieRepository $categorieRepository): JsonResponse
    {
        $content = json_decode($request->getContent(), false, 512, JSON_THROW_ON_ERROR);
        $invention = new Invention();
        $this->setDataInvention($invention, $content, $categorieRepository, $inventeurRepository);
        $inventionsRepository->save($invention, true);
        return $this->json($invention, 201, [], ['groups' => 'inventions:ajout']);
    }

    /**
     * @throws \JsonException
     * @throws \Exception
     */
    #[Route('/{id}', name: 'app_edit_inventions', methods: ['PUT'])]
    public function editInventions(Request $request, InventionsRepository $inventionsRepository, InventeurRepository $inventeurRepository, CategorieRepository $categorieRepository, int $id): JsonResponse
    {
        $invention = $inventionsRepository->find($id);
        if (!$invention) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }
        $content = json_decode($request->getContent(), false, 512, JSON_THROW_ON_ERROR);
        $this->setDataInvention($invention, $content, $categorieRepository, $inventeurRepository);
        $inventionsRepository->save($invention, true);
        return $this->json($invention, 201, [], ['groups' => 'inventions:ajout']);
    }

    #[Route('/{id}', name: 'app_delete_inventions', methods: ['DELETE'])]
    public function deleteInventions(InventionsRepository $inventionsRepository, int $id): JsonResponse
    {
        $invention = $inventionsRepository->find($id);
        if (!$invention) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }
        $inventionsRepository->remove($invention, true);
        return $this->json([], 204);
    }

    /**
     * @param Invention $invention
     * @param mixed $content
     * @param CategorieRepository $categorieRepository
     * @param InventeurRepository $inventeurRepository
     * @return void
     * @throws \Exception
     */
    private function setDataInvention(Invention $invention, mixed $content, CategorieRepository $categorieRepository, InventeurRepository $inventeurRepository): void
    {
        $invention->setName($content->name);
        $invention->setDescription($content->description);
        $invention->setAnnee(new \DateTime($content->date));
        $invention->setImage($content->image);
        $invention->setCategorie($categorieRepository->find($content->categorie));
        $invention->removeAllInventeur();
        foreach ($inventeurRepository->findById($content->inventeur) as $inventeur) {
            $invention->addInventeur($inventeur);
        }
    }

}
