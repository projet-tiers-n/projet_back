<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Categorie;
use App\Repository\CategorieRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/categories')]
class CategorieController extends AbstractController
{
    #[Route('/', name: 'app_list_categories', methods: ['GET'])]
    public function getCategories(CategorieRepository $categorieRepository): JsonResponse
    {
        return $this->json($categorieRepository->findAll(), 200, [], ['groups' => 'categories:read:list']);
    }

    #[Route('/ids', name: 'app_ids_categories', methods: ['GET'])]
    public function getIdsCategorie(CategorieRepository $categorieRepository): JsonResponse
    {
        return $this->json($categorieRepository->findAll(), 200, [], ['groups' => 'categories:read:ids']);
    }

    #[Route('/{id}', name: 'app_get_categories', methods: ['GET'])]
    public function getCategorie(CategorieRepository $categoriesRepository, int $id): JsonResponse
    {
        return $this->json($categoriesRepository->find($id), 200, [], ['groups' => 'categories:read:item']);
    }

    #[Route('/', name: 'app_add_categories', methods: ['POST'])]
    public function addCategorie(Request $request, CategorieRepository $categorieRepository): JsonResponse
    {
        $content = json_decode($request->getContent(), false, 512, JSON_THROW_ON_ERROR);
        $categorie = new Categorie();
        $categorie->setName($content->name);
        $categorieRepository->save($categorie);
        return $this->json($categorie, 201, [], ['groups' => 'categories:ajout']);
    }

    /**
     * @throws \JsonException
     */
    #[Route('/{id}', name: 'app_edit_categories', methods: ['PUT'])]
    public function editCategorie(Request $request, CategorieRepository $categorieRepository, int $id): JsonResponse
    {
        $categorie = $categorieRepository->find( $id);
        if (!$categorie){
            throw $this->createNotFoundException(
                'No categorie found for id '.$id
            );
        }
        $content = json_decode($request->getContent(), false, 512, JSON_THROW_ON_ERROR);
        $categorie->setName($content->name);
        $categorieRepository->save($categorie, true);
        return $this->json($categorie, 200, [], ['groups' => 'categories:read:item']);
    }

    #[Route('/{id}', name: 'app_delete_categories', methods: ['DELETE'])]
    public function deleteCategorie(CategorieRepository $categorieRepository, int $id): JsonResponse
    {
        $categorie = $categorieRepository->find( $id);
        if (!$categorie){
            throw $this->createNotFoundException(
                'No categorie found for id '.$id
            );
        }
        $categorieRepository->remove($categorie, true);
        return $this->json([], 204);
    }

}
