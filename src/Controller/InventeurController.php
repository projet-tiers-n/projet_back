<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Inventeur;
use App\Repository\InventeurRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/inventeurs')]
class InventeurController extends AbstractController
{
    #[Route('/', name: 'app_list_inventeur',methods: ['GET'])]
    public function listInventeur(Request $request, InventeurRepository $inventeurRepository): JsonResponse
    {
        $pays = $request->query->get('pays');
        return $this->json($inventeurRepository->findByPays($pays), 200, [], ['groups' => 'inventeurs:read:list']);
    }

    #[Route('/ids', name: 'app_id_inventeurs', methods: ['GET'])]
    public function getIdInventeurs(InventeurRepository $inventeurRepository): JsonResponse
    {
        return $this->json($inventeurRepository->findAll(), 200, [], ['groups' => 'inventeurs:read:id']);
    }

    #[Route('/{id}', name: 'app_get_inventeur', methods: ['GET'])]
    public function getInventeur(InventeurRepository $inventeurRepository, int $id): JsonResponse
    {
        return $this->json($inventeurRepository->find($id), 200, [], ['groups' => 'inventeurs:read:item']);
    }


    /**
     * @throws \JsonException
     */
    #[Route('/', name: 'app_add_inventeurs', methods: ['POST'])]
    public function addInventeur(Request $request, InventeurRepository $inventeurRepository): JsonResponse
    {
        $content = json_decode($request->getContent(), false, 512, JSON_THROW_ON_ERROR);
        $inventeur = new Inventeur();
        $inventeur->setName($content->name);
        $inventeur->setPays($content->pays);
        $inventeur->setDescription($content->description);
        $inventeurRepository->save($inventeur, true);
        return $this->json($inventeur, 201, [], ['groups' => 'inventeurs:read:item']);
    }

    /**
     * @throws \JsonException
     */
    #[Route('/{id}', name: 'app_edit_inventeurs', methods: ['PUT'])]
    public function editInventeur(Request $request, InventeurRepository $inventeurRepository, int $id): JsonResponse
    {
        $inventeur = $inventeurRepository->find( $id);
        if (!$inventeur){
            throw $this->createNotFoundException(
                'No inventeur found for id '.$id
            );
        }
        $content = json_decode($request->getContent(), false, 512, JSON_THROW_ON_ERROR);
        $inventeur->setName($content->name);
        $inventeur->setPays($content->pays);
        $inventeur->setDescription($content->description);
        $inventeurRepository->save($inventeur, true);
        return $this->json($inventeur, 200, [], ['groups' => 'inventeurs:read:item']);
    }

    #[Route('/{id}', name: 'app_delete_inventeurs', methods: ['DELETE'])]
    public function deleteInventeur(InventeurRepository $inventeurRepository, int $id): JsonResponse
    {
        $inventeur = $inventeurRepository->find( $id);
        if (!$inventeur){
            throw $this->createNotFoundException(
                'No inventeur found for id '.$id
            );
        }
        $inventeurRepository->remove($inventeur, true);
        return $this->json([], 204);
    }
}
