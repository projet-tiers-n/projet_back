# My_Projet_Back - API Symfony 6

Ce dépôt contient le code source de l'API Symfony développée comme Back-End d'une application musée d'invention inutile. L'API communique avec une base de données MariaDB et fournit les fonctionnalités nécessaires pour gérer les données de l'application front end.

Ce projet a été une véritable occasion pour moi d'appliquer toutes mes connaissances existantes tout en enrichissant mon expérience avec de nouvelles compétences essentielles dans la gestion d'un projet d'application web. En travaillant sur cette API Symfony 6 et en collaborant au développement avec une base de données MariaDB, j'ai non seulement approfondi ma compréhension de la conception logicielle et de la gestion de versions, mais j'ai aussi eu l'opportunité de résoudre des problèmes complexes. Cette expérience m'a également permis de découvrir les défis spécifiques liés au déploiement sur des plateformes cloud telles que Heroku, renforçant ainsi ma capacité à naviguer efficacement dans le monde du développement web moderne.

## Contexte du Projet

Ce projet a été réalisé dans le cadre d'un cours où nous devions développer une application complète en architecture N-tier. L'objectif principal était d'experimenter les différentes étapes d'un projet, de la schématisation au déploiement.


## Fonctionnalités Clés

- Gestion des Inventions: CRUD (Create, Read, Update, Delete) pour les inventions inutiles.
- Endpoints API: Exposition d'endpoints RESTful pour interagir avec les données des inventions.

## Technologies Utilisées

- Symfony 6: Framework PHP utilisé pour construire l'API.
- MariaDB: Système de gestion de base de données relationnelle utilisé pour stocker les données des inventions.
- Docker: Utilisé pour encapsuler l'application Symfony et la base de données MariaDB, facilitant ainsi le déploiement et l'environnement de développement.
- Heroku: Plateforme d'hébergement utilisée pour déployer l'application.

## Liens Utiles

- Front End Repository: [Projet Front End React](https://gitlab.com/projet-tiers-n/my_project_front)

Contribution

Ce projet a été développé par Florian Huteau, dans le cadre d'un projet académique visant à explorer et à appliquer les concepts de développement web moderne.