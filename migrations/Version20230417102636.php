<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230417102636 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE inventeur DROP FOREIGN KEY FK_642980AFA6E44244');
        $this->addSql('DROP TABLE pays');
        $this->addSql('DROP INDEX IDX_642980AFA6E44244 ON inventeur');
        $this->addSql('ALTER TABLE inventeur ADD pays VARCHAR(2) NOT NULL, DROP pays_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE pays (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(2) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, nombre_invention INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE inventeur ADD pays_id INT NOT NULL, DROP pays');
        $this->addSql('ALTER TABLE inventeur ADD CONSTRAINT FK_642980AFA6E44244 FOREIGN KEY (pays_id) REFERENCES pays (id)');
        $this->addSql('CREATE INDEX IDX_642980AFA6E44244 ON inventeur (pays_id)');
    }
}
