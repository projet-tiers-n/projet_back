<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230414144452 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE categorie (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE inventeur (id INT AUTO_INCREMENT NOT NULL, pays_id INT NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_642980AFA6E44244 (pays_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE invention (id INT AUTO_INCREMENT NOT NULL, categorie_id INT NOT NULL, name VARCHAR(50) NOT NULL, description VARCHAR(255) NOT NULL, annee DATE NOT NULL, INDEX IDX_E0139CB1BCF5E72D (categorie_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE invention_inventeur (invention_id INT NOT NULL, inventeur_id INT NOT NULL, INDEX IDX_95B4E040FB97D53F (invention_id), INDEX IDX_95B4E040CD54DF2E (inventeur_id), PRIMARY KEY(invention_id, inventeur_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pays (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(2) NOT NULL, nombre_invention INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE inventeur ADD CONSTRAINT FK_642980AFA6E44244 FOREIGN KEY (pays_id) REFERENCES pays (id)');
        $this->addSql('ALTER TABLE invention ADD CONSTRAINT FK_E0139CB1BCF5E72D FOREIGN KEY (categorie_id) REFERENCES categorie (id)');
        $this->addSql('ALTER TABLE invention_inventeur ADD CONSTRAINT FK_95B4E040FB97D53F FOREIGN KEY (invention_id) REFERENCES invention (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE invention_inventeur ADD CONSTRAINT FK_95B4E040CD54DF2E FOREIGN KEY (inventeur_id) REFERENCES inventeur (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE inventeur DROP FOREIGN KEY FK_642980AFA6E44244');
        $this->addSql('ALTER TABLE invention DROP FOREIGN KEY FK_E0139CB1BCF5E72D');
        $this->addSql('ALTER TABLE invention_inventeur DROP FOREIGN KEY FK_95B4E040FB97D53F');
        $this->addSql('ALTER TABLE invention_inventeur DROP FOREIGN KEY FK_95B4E040CD54DF2E');
        $this->addSql('DROP TABLE categorie');
        $this->addSql('DROP TABLE inventeur');
        $this->addSql('DROP TABLE invention');
        $this->addSql('DROP TABLE invention_inventeur');
        $this->addSql('DROP TABLE pays');
    }
}
