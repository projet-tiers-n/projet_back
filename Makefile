help:
	@echo "Commandes dans le projet"
	@echo ""
	@echo "1) Commandes Docker"
	@echo "-------------------"
	@echo "up       créé les container					docker compose up -d"
	@echo "down     stop les container et les détruits			docker compose down"
	@echo ""
	@echo "2) Commandes Composer"
	@echo "---------------------"
	@echo "composer_install		install les dépendances		docker compose exec php composer install"
	@echo ""
	@echo "3) Commandes Symfony"
	@echo "---------------------"
	@echo "entity		génère une entité				docker compose exec php php bin/console make:entity"
	@echo "migrate		migre la bdd					docker compose exec php php bin/console d:m:diff/d:m:m"

up:
	docker compose up -d

down:
	docker compose down

composer_install:
	docker compose exec php composer install

composer_install_package:
	docker compose exec php composer require $(filter-out $@,$(MAKECMDGOALS))

entity:
	docker compose exec php php bin/console make:entity

migrate:
	docker compose exec php php bin/console make:migration
	docker compose exec php php bin/console d:m:m -n


import_donnees:
	docker compose exec php php bin/console dbal:run-sql "$$(cat docker/donnees.sql)"

db-reset:
	docker compose exec php php bin/console d:d:d --force
	docker compose exec php php bin/console d:d:c
	docker compose exec php php bin/console d:m:m
	docker compose exec php php bin/console dbal:run-sql "$$(cat docker/donnees.sql)"